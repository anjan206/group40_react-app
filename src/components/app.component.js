import React from 'react';
import ReactDOM from 'react-dom';
import { Login } from './auth/login/login.component';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Header } from './common/header/header.component';

const mount = ()=>{
    ReactDOM.render( <Login/>,document.getElementById('next'))
}
const unmount = ()=>{
    ReactDOM.unmountComponentAtNode(document.getElementById('next'));
}


// functional component
export const App = (args)=>{
    console.log(args);
    // functional component must return single html node

    return (
        <div>
        {/* <p>i am root  component</p>
        <p></p>
        <p>i am root cdfdomponent</p> */}
        <Header isLoggedIn={true}/>
        {/* <Header isLoggedIn={false}/> */}
        <button onClick={mount}>MOunt</button>
        <button onClick={unmount}>Unmount</button>
        <div id="next"></div>
        {/* <Login/> */}
        </div>
    )
}