import React, {Component } from 'react';

export class Login extends Component {
    constructor(){
        super();
        this.state={
            username: '',
            password:'',
            usernameErr:'',
            passwordErr:'',
            remember_me:'',
            isSubmitting:''
            // initial state
        }
    }
    componentDidMount(){

    }
    componentDidUpdate(prevProps, prevState){

    }
    componentWillUnmount(){

    }

    handleChange=(e)=>{
        let {name, value} = e.target;
        // console.log('message is >>>',err);
        console.log('name is >>',name)
        console.log('value is >>',value);
        this.setState({
            [name]: value
        }, ()=>{
            // form validation here
            console.log('this.state>>', this.state)
        })
    }

    handleSubmit = (e)=>{
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        // API CALL
        setTimeout(()=>{
            this.setState({
                isSubmitting: false
            })
        },5000)
        console.log('this.submit', this.state)
    }

    render(){
        let btn = this.state.isSubmitting
        ?<button disabled className="btn btn-info">Login....n</button>
        :<button type="submit" className="btn btn-primary">Loginbuton</button>


        return (
            <div>
                <h2>Login</h2>
                <p>Please login ot continue</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label htmlFor="uname">Username</label>
                    <input className= "form-control" type="text" name="username" id="uname" placeholder="Username enter" onChange={this.handleChange}/>
                    <label htmlFor="pwd">Password</label>
                    <input className="form-control" type="password" name="password" id="pwd" placeholder="password enter" onChange={this.handleChange} />
                    <hr />
                    {btn}
                    <hr />
                    <p>Don't have an account</p>
                    <p style={{float:'left'}}>Register <a href="/register">Here</a></p>
                    <p style={{float:'right'}}><a href="`/forgert_password">Forget Password?</a></p>
                </form>
            </div>
        )
    }
}